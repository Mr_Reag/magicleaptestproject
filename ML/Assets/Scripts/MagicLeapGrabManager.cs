﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;
using System.Linq;


[RequireComponent(typeof(HandTracking))]
public class MagicLeapGrabManager : MonoBehaviour
{
    public static MagicLeapGrabManager Instance { get; private set; }
    private static object l = new object();

    //setup singleton
    void Awake()
    {
        lock (l)
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                Instance = this;
                DontDestroyOnLoad(Instance);
            }
        }
    }

    void Start()
    {
        if (!GetComponent<HandTracking>().enabled)
        {
            Debug.LogError("Error with HandTracking script!");
            Destroy(this);
            return;
        }

        //register our events with the ML API
        MLHands.Left.OnKeyPoseBegin += OnLeftPose;
        MLHands.Right.OnKeyPoseBegin += OnRightPose;
        MLHands.Left.OnKeyPoseEnd += OnLeftPoseEnd;
        MLHands.Right.OnKeyPoseEnd += OnRightPoseEnd;

    }


    /// <summary>
    /// Called when a Pose ends
    /// </summary>
    /// <param name="obj"></param>
    private void OnRightPoseEnd(MLHandKeyPose pose)
    {
        //if the pose isnt a pinch, stop
        if (pose != MLHandKeyPose.Pinch && pose != MLHandKeyPose.Fist)
        {
            return;
        }

        var grabsInScene = FindObjectsOfType<Interaction>().ToList();
        grabsInScene.ForEach(x => x.OnGrabDisengaged(MLHands.Right));
    }

    private void OnLeftPoseEnd(MLHandKeyPose pose )
    {
        //if the pose isnt a pinch, stop
        if (pose != MLHandKeyPose.Pinch && pose != MLHandKeyPose.Fist)
        {
            return;
        }

        var grabsInScene = FindObjectsOfType<Interaction>().ToList();
        grabsInScene.ForEach(x => x.OnGrabDisengaged(MLHands.Left));
    }

    /// <summary>
    /// Called when we see a pose starting
    /// </summary>
    /// <param name="pose">The pose we saw</param>
    private void OnRightPose(MLHandKeyPose pose)
    {
        //if the pose isnt a pinch, stop
        if (pose != MLHandKeyPose.Pinch && pose != MLHandKeyPose.Fist)
        {
            return;
        }

        

        //Otherwise, begin the grab
        MLHand right = MLHands.Right;
        var loc = right.Center;
        var target = GetClosestGrabbable(loc);
        if (target == null) return;
        target.gameObject.GetComponent<Interaction>().OnGrabEngage(right);

    }

    private void OnLeftPose(MLHandKeyPose pose)
    {


        //if the pose isnt a pinch, stop
        if (pose != MLHandKeyPose.Pinch && pose != MLHandKeyPose.Fist)
        {
            return;
        }

        //Otherwise, begin the grab
        MLHand left = MLHands.Left;
        var loc = left.Index.Tip.Position; //calculate closest from the tip of the index finger, rather than the center of the hand
        var target = GetClosestGrabbable(loc);
        if (target == null) return;
        target.gameObject.GetComponent<Interaction>().OnGrabEngage(left);
    }

    /// <summary>
    /// Finds the closest grab to a particular location
    /// </summary>
    /// <returns>the closest object of Interaction type, or null if they are all too far</returns>
    private static Transform GetClosestGrabbable(Vector3 currentPosition)
    {
        var grabsInScene = FindObjectsOfType<Interaction>().Select(x => x.transform).ToArray();

        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        foreach (Transform potentialTarget in grabsInScene)
        {
            Vector3 directionToTarget = potentialTarget.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                float MaxDist = potentialTarget.gameObject.GetComponent<Interaction>().InteractionDistance;
                MaxDist *= MaxDist; //sqr so we keep the values equal
                //if we're too far away from the target to interact, skip it
                if (dSqrToTarget > MaxDist) continue;

                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }
        
        //returns the best target, which can be null if none are found
        return bestTarget;
    }
}
