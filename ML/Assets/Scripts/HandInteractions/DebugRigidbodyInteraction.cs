﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;

public class DebugRigidbodyInteraction : RigidbodyInteraction
{


    private MLHand _hand;
    private Vector3 _localOffset;

    protected override void Engage()
    {
        _hand = GrabbingHands[0];

        PrepareRigidbodyForInteraction();

        // Store the offset of the object local to the hand feature.  This will be used to keep the object at the same distance from the hand when being moved.

        SetHandOffsets();
    }

    /// <inheritdoc />
    /// <summary>
    /// We check if we can engage the grab. For this interaction, we only allow one hand to grab at a time!
    /// </summary>
    protected override bool CanEngage(MLHand hand)
    {
        return GrabbingHands.Count == 1;
    }

    private void SetHandOffsets()
    {
        _localOffset = TargetTransform.position - _hand.Center;
        SetGrabOffset(_hand.Center);
    }



    protected override void Disengage()
    {
        ManipulateTarget();
        RestoreRigidbodySettingsAfterInteraction();
        _hand = null;
    }

    /// <inheritdoc />
    /// <summary>
    /// Only the owner hand can disengage the grab. This allows for us to transfer the grab to another hand and release smoothly
    /// </summary>
    protected override bool CanDisengage(MLHand hand)
    {
        return hand.Equals(GrabbingHands[0]);
    }

    protected override void ManipulateTarget()
    {
        Move(TransformedHandPos());
    }

    private Vector3 TransformedHandPos()
    {
     
        Vector3 grabPosition = _hand.Center;
        return grabPosition;
    }
}
