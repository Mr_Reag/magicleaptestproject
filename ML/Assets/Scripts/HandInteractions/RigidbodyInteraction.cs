﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public abstract class RigidbodyInteraction : Interaction
{

    protected Rigidbody _rigidbody;
    private bool _wasKinematic;
    private RigidbodyConstraints _priorConstraints;
    private bool _usedGravity;

    /// <summary>
    /// Get the rigid body of the target transform, so we will be abl to manipulate it later
    /// </summary>
    protected virtual void Awake()
    {
        _rigidbody = TargetTransform.gameObject.GetComponent<Rigidbody>();
    }

    /// <summary>
    /// Set the rigidbody to kinematic and clear its constraints so as not to interfere with interaction translation.
    /// </summary>
    protected void PrepareRigidbodyForInteraction()
    {
        if (_rigidbody != null)
        {
            _wasKinematic = _rigidbody.isKinematic;
            _priorConstraints = _rigidbody.constraints;
            _usedGravity = _rigidbody.useGravity;

            ChangeRigidbodyForInteraction();
        }
    }

    protected virtual void ChangeRigidbodyForInteraction()
    {
        _rigidbody.constraints = RigidbodyConstraints.None;
        _rigidbody.isKinematic = true;
        _rigidbody.useGravity = false;
    }

    /// <summary>
    /// Restore the rigidbody's kinematic state and constraints as they were prior to the most recent interaction.
    /// </summary>
    protected void RestoreRigidbodySettingsAfterInteraction()
    {
        if (_rigidbody != null)
        {
            _rigidbody.isKinematic = _wasKinematic;
            _rigidbody.constraints = _priorConstraints;
            _rigidbody.useGravity = _usedGravity;
        }
    }


    protected override void Engage()
    {
        PrepareRigidbodyForInteraction();
    }

    protected override void Disengage()
    {
        RestoreRigidbodySettingsAfterInteraction();
    }
}
