﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;


public abstract class Interaction : MonoBehaviour
{

    #region Vars
    [SerializeField]
    private Transform _targetTransform;

    [SerializeField] private float _interactionDist = .1F;



    protected List<MLHand> GrabbingHands = new List<MLHand>();
    protected List<MLHand> HoveringHands = new List<MLHand>();



    private bool _isActive = false;

    protected bool isHoveredUpon;

    /// <summary>
    /// Offset of the grabbing hand/s from the main affected transform at the moment of grab.
    /// </summary>
    protected Vector3 GrabOffset;

    #endregion

    #region Definitions and Unity Native


    /// <summary>
    /// This field encapsulated the target transformation for our manipulation
    /// </summary>
    public Transform TargetTransform
    {
        get
        {
            if (_targetTransform == null)
            {
                return transform;
            }
            return _targetTransform;
        }
        set { _targetTransform = value; }
    }

    /// <summary>
    /// This field encapsulates the maximum distance that this object will detect a grab from
    /// </summary>
    public float InteractionDistance
    {
        get { return _interactionDist; }
        private set { _interactionDist = value; }
    }

    /// <summary>
    /// When the grab is active, perform the manipulation function every frame
    /// TODO: Check if this is too performance heavy, and if it should be replaced with a coroutine
    /// </summary>
    protected virtual void Update()
    {
        if (_isActive)
        {
            ManipulateTarget();
        }
    }
#endregion

    #region GrabMethods
    /// <summary>
    /// Called when grabbing is attempted
    /// </summary>
    /// <param name="grabbingHand">The hand that grabs</param>
    public void OnGrabEngage(MLHand grabbingHand)
    {
        if (enabled)
        {
            GrabbingHands.Add(grabbingHand);
            StartGrabEngage(grabbingHand);
        }
    }

    /// <summary>
    /// Check to make sure we can, infact, engage the grab. The CanEngage function can be overwritte by subclasses
    /// </summary>
    /// <param name="grabbingHand"></param>
    private void StartGrabEngage(MLHand grabbingHand)
    {
        if (CanEngage(grabbingHand))
        {
            OnEngaged(grabbingHand);
        }
    }

    /// <summary>
    /// Do the actual grab Activate the grab functiosn and Engage! </summary>
    /// <param name="grabbingHand"></param>
    private void OnEngaged(MLHand grabbingHand)
    {
        _isActive = true;
        Engage();
    }


    /// <summary>
    /// Called when a grab ends
    /// </summary>
    /// <param name="releasingHand"></param>
    public void OnGrabDisengaged(MLHand releasingHand)
    {
        if (enabled)
        {
            // -- Ensure this hand is grabbing object in first place
            if (!GrabbingHands.Contains(GrabbingHands.Find(hand => hand == releasingHand)))
            {
                return;
            }

            StartGrabDisengage(releasingHand);

            // -- Remove Grabbing Hand
            GrabbingHands.Remove(GrabbingHands.Find(hand => hand == releasingHand));
        }
    }

    /// <summary>
    /// Check to make sure we can, infact, disengage the grab. The CanDisngage function can be overwritte by subclasses
    /// </summary>
    /// <param name="releasingHand"></param>
    private void StartGrabDisengage(MLHand releasingHand)
    {
        if (CanDisengage(releasingHand))
        {
            OnDisengaged(releasingHand);
        }
    }

    /// <summary>
    /// Actually performs the disengage and flips the flag to false
    /// </summary>
    /// <param name="releasingHand"></param>
    private void OnDisengaged(MLHand releasingHand)
    {
        _isActive = false;
        Disengage();
    }
    #endregion

    #region HelperMethods
    /// <summary>
    /// Returns the current state of the Interaction
    /// </summary>
    /// <returns>if this interaction is active or not</returns>
    public bool IsActive()
    {
        return _isActive;
    }

    /// <summary>
    /// Calculates the difference in position between the grabbing hand and the GameObject
    /// </summary>
    protected void SetGrabOffset(Vector3 handPosition)
    {
        GrabOffset = TargetTransform.transform.position - handPosition;
    }

    /// <summary>
    /// Translate target transform.
    /// </summary>
    protected void Move(Vector3 position)
    {
        TargetTransform.position = position + GrabOffset;
    }

    /// <summary>
    /// Rotate target transform.
    /// </summary>
    protected void Rotate(Quaternion rotation)
    {
        TargetTransform.rotation = rotation;
    }


    /// <summary>
    /// Returns true when proper conditions are met to engage this manipulation.
    /// </summary>
    protected virtual bool CanEngage(MLHand hand) { return true; }

    /// <summary>
    /// Called when Engaged.
    /// </summary>
    protected abstract void Engage();

    /// <summary>
    /// Returns true when proper conditions are met to disengage this manipulation.
    /// </summary>
    protected virtual bool CanDisengage(MLHand hand) { return true; }

    /// <summary>
    /// Called when Disengaged.
    /// </summary>
    protected abstract void Disengage();

    /// <summary>
    /// Called every frame to perform manipulation.
    /// </summary>
    protected abstract void ManipulateTarget();
#endregion

}
