﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugInteraction : Interaction
{
    protected override void Disengage()
    {
        Debug.Log("Disengaged");
    }

    protected override void Engage()
    {
       Debug.Log("Engaged");
    }

    protected override void ManipulateTarget()
    {
        Debug.Log("Manipulate");
    }
}
